from types import SimpleNamespace
from .classes.Styles import Styles

import re
import aiohttp
import async_timeout
import json
from .classes.Builds import Builds

_re = SimpleNamespace(
    url=re.compile(r"(?<!<)(https?://(?:[a-z]+.)?pcpartpicker.com/"
                   "(list|user/[A-Za-z0-9]+/saved)/([A-Za-z0-9]+){0,6})(?!>)", re.IGNORECASE | re.DOTALL),
    format=re.compile(r"^([A-Za-z ]+): ([A-Za-z0-9()!/+'.&;\s\-\(\"]+) (?:\((?:Purchased For )?(.)([0-9.]+).*)?$",
                      re.IGNORECASE | re.MULTILINE),
    saved=SimpleNamespace(
        id=re.compile(r"init_partlist_save_dialog\('..', '(\S+){0,6}'\);", re.IGNORECASE | re.DOTALL),
        userinfo=re.compile(r"<div class=\"avatar-username\">.*<h1>([\s\S]+)</h1>"
                            r".*<span>(\S+)</span>.*?</div>", re.IGNORECASE | re.DOTALL)
    )
)


async def get_build(build_id):
    async with aiohttp.ClientSession() as session:
        async with async_timeout.timeout(15):
            async with session.get("https://pcpartpicker.com/qapi/partlist/markup/?mode=plaintext&"
                                   f"select=preferred&merchant=-1&partlist={build_id}&parametric_tags=") as response:
                text = await response.text()
                data = json.loads(text)["content"]

    part_list = []
    for match in _re.format.finditer("\n".join(data.split("\\n"))):
        price = match.group(3)
        if price:
            prices = SimpleNamespace(
                cur=match.group(3),
                amt=match.group(4)
            )
        else:
            prices = SimpleNamespace(
                cur=" ",
                amt="N/A"
            )

        part_list.append(SimpleNamespace(
            type=match.group(1),
            name=(match.group(2)).replace("&quot;", '"').replace("&amp;", "&"),
            price=prices
        ))

    return part_list


async def format_build(url, style=None, escape=False, saved=False):
    """
    Return a list of pages (max 2000 char) with the PC Part Picker specifications

    :param url: the full URL (if it's a saved build) or plain ID of the build
    :param style: the styling of the build
    :param escape: whether or not to escape all styling
    :param saved: whether or not this is a saved build
    :return: List of strings
    """
    if saved:
        async with aiohttp.ClientSession() as session:
            async with async_timeout.timeout(15):
                async with session.get(url) as response:
                    text = await response.text()

                    user_info = _re.saved.userinfo.findall(text)
                    build_name = user_info[0][0]
                    user = user_info[0][1]

                    build_id = _re.saved.id.findall(text)
                    url = build_id[0]

    parts = await get_build(url)
    parts = getattr(Styles, style.lower())(parts, escape)
    if saved:
        return [user + "'s " + build_name] + parts
    else:
        return parts
