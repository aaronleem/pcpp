from .Build import Build


class Builds:
    def __init__(self, arango_db):
        self.state = arango_db["dino"]
        self.build_list_cache = []

    def __len__(self):
        return len(self.build_list_cache)

    def add_build(self, user_id, build_name, component_list):

        pass

    def remove_build(self, user_id, build_id):
        pass

    def get_build_by_id(self, build_id):
        pass

    def get_build_by_name(self, build_name):
        pass

    def get_build(self, build_identifier):
        pass


