from .Component import Component


class Build:
    def __init__(self, name: str, user_id: int, pcpp_id: str):
        self._name = name
        self._discord_user_id = user_id
        self._build_id = None
        self._pcpp_id = pcpp_id
        self._component_list = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def owner(self):
        return self._discord_user_id

    @property
    def id(self):
        return self._build_id

    def add_component(self, component_name, component_product_info):
        self._component_list.append(Component(component_product_info, component_name))

    def update_component(self, component_name, component_product_info):
        pass
