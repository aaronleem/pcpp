from types import SimpleNamespace
from discord.ext.commands import Paginator


class Styles:
    @staticmethod
    def create(
            components, escaped, styling,
            type_pad_formatter, price_pad_formatter, formatter, total_price_formatter
    ):
        """
        Creates a paginated view of all items in the pcpp build
        Uses discord.ext.commands.Paginator

        :param components: list of all components
        :param escaped: block quotes or escaped block quotes
        :param styling: the style to be used for the block quote
        :param type_pad_formatter: the formatter for the product type padding (spacing)
        :param price_pad_formatter: the formatter for the product price padding (spacing)
        :param formatter: the formatter for the list of products
        :param total_price_formatter: the total price formatter
        :return: List of pages
        """
        pad = SimpleNamespace(type=12, price=0)
        for i in components:
            # Find out which has the longest
            temp_len = len(type_pad_formatter(i.type)) + 1
            if temp_len > pad.type:
                pad.type = temp_len

            temp_len = len(price_pad_formatter(i.price.amt))
            if temp_len > pad.price:
                pad.price = temp_len

        formatted = Paginator(prefix=escaped + styling, suffix=escaped)

        total_price = sum(float(i.price.amt) for i in components if i.price.cur != ' ')
        currency = next((i.price.cur for i in components if i.price.cur != ' '), "")

        for i in components:
            formatted.add_line(formatter(i, pad, i.name[:50] + '..' * (len(i.name) > 50)))

        formatted.add_line(total_price_formatter(total_price, currency, pad))
        return formatted.pages

    @staticmethod
    def md(items, escaped=False):
        return Styles.create(
            items, '\`\`\`' if escaped else '```', 'md',

            lambda t:           f"<{t.replace(' ', '-')}:",
            lambda p:           f"{p}",
            lambda t, p, n:     f"{t.type.replace(' ', '-').rjust(p.type, ' ')} : "
                                f"$ {t.price.rjust(p.price, ' ')} : {n}",
            lambda pr, cur, pd: f"{'Total price'.rjust(pd.type, ' ')} : "
                                f"{cur + format(pr,'.2f')}"
        )

    @staticmethod
    def md_block(items, escaped=False):
        return Styles.create(
            items, '\`\`\`' if escaped else '```', 'md',

            lambda t:           f"[{t}",
            lambda p:           f"$ {p}",
            lambda t, p, n:     f"| {('[ '+t.type).rjust(p.type, ' ')} ]"
                                f"[ {t.price.cur + t.price.amt.rjust(p.price, ' ')} : {n} ]",
            lambda pr, cur, pd: f"| {'[ Total price'.rjust(pd.type, ' ')} ]"
                                f"[ {cur + format(pr,'.2f').rjust(pd.price, ' ')} ]"
        )

    @staticmethod
    def bold(items, escaped=False):
        formatted = Paginator(prefix="", suffix="")
        escaped = "\*\*" if escaped else "**"
        total = 0
        for i in items:
            total += float(i.price)
            formatted.add_line(f"{escaped}{i.type}:{escaped} {i.name} ($ {i.price})")
        formatted.add_line(f"{escaped}Total price:{escaped} $ {format(total, '.2f')}")
        return formatted.pages

    @staticmethod
    def bold_ital(items, escaped=False):
        formatted = Paginator(prefix="", suffix="")
        itals = "\*" if escaped else "*"
        bolds = "\*\*\*" if escaped else "***"
        total = 0
        for i in items:
            total += float(i.price)
            formatted.add_line(f"{bolds}{i.type}:{bolds} {itals}{i.name} ($ {i.price}){itals}")
        formatted.add_line(f"{escaped}Total price:{escaped} {format(total, '.2f')}")
        return formatted.pages
