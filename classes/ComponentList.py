from .Component import Component


class ComponentList(list):
    def append(self, obj: Component):
        try:
            item: Component = next(filter(lambda c: c.type == obj.type, self))
        except StopIteration:
            super().append(obj)
        else:
            # update instead of append
            item.type = obj.type

    def remove(self, obj: Component):
        try:
            index: int = next(i for i, c in enumerate(self) if c.type == obj.type)
        except StopIteration:
            return

        super().__delitem__(index)

    def __getitem__(self, item):
        try:
            if isinstance(item, str):
                item = next(filter(lambda c: c.type == item, self))
            elif isinstance(item, int):
                item = super().__getitem__(item - 1)
        except StopIteration:
            return None
        else:
            return item


t = ComponentList()
t.append(Component("CPU", "poop"))
print(t["CPU"].name)
print(t[1].name)
t.remove(t["CPU"])
print(t)
